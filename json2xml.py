import argparse
import json
import xml.etree.ElementTree as ET

def json_to_xml(json_data):
    root = ET.Element("root")
    parse_json(root, json_data)
    xml_tree = ET.ElementTree(root)
    return xml_tree

def parse_json(parent, json_data):
    if isinstance(json_data, dict):
        for key, value in json_data.items():
            if isinstance(value, (dict, list)):
                element = ET.SubElement(parent, key)
                parse_json(element, value)
            else:
                ET.SubElement(parent, key).text = str(value)
    elif isinstance(json_data, list):
        for item in json_data:
            parse_json(parent, item)

parser = argparse.ArgumentParser(description="Convert JSON to XML")
parser.add_argument("-o", "--output", help="Output XML file path", required=True)
parser.add_argument("-i", "--input", help="Input JSON file path", required=True)
args = parser.parse_args()

output_file = args.output
input_file = args.input

with open(input_file, "r") as f:
    json_data = json.load(f)

xml_tree = json_to_xml(json_data)
xml_tree.write(output_file, encoding="utf-8", xml_declaration=True)
