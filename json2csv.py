import json
import csv
import argparse

def json_to_csv(input_file, output_file):
    # Open the input JSON file in read mode
    with open(input_file, 'r') as json_file:
        # Load the JSON data into a Python object
        data = json.load(json_file)

    # Open the output CSV file in write mode
    with open(output_file, 'w', newline='') as csv_file:
        # Create a CSV writer object
        writer = csv.writer(csv_file)

        # Write the header row with the keys from the first item in the data list
        writer.writerow(data[0].keys())

        # Write the remaining rows with the values from each item in the data list
        writer.writerows(item.values() for item in data)

# Parse command-line arguments
parser = argparse.ArgumentParser(description='Convert files JSON into CSV')
parser.add_argument('--in', dest='input_file', required=True, help='Input JSON-file')
parser.add_argument('--out', dest='output_file', required=True, help='Output CSV-file')
args = parser.parse_args()

# Convert JSON-file to CSV-file
json_to_csv(args.input_file, args.output_file)


